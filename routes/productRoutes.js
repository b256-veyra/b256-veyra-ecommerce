const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Route for creating a product
router.post("/create", auth.verify, (req, res) => {

	// contains all the information needed in the function
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);

	}
});

// Route for retrieving all product
router.get("/all", (req, res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController));

})
 

// Route for retrieving all active product
router.get("/active-products", (req, res) => {

	productController.getActiveProduct().then(resultFromController => res.send(resultFromController));

});


// Route for retrieving a specific product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

router.put("/update/:productId", auth.verify, (req, res) => {

	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);

	}
})

// Route to archiving a product
router.put("/archive/:productId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});

router.put("/reactivate/:productId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});


module.exports = router;