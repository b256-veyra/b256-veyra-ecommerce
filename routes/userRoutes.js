const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Register a User
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Authenticate a user
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});
 

// Route for creating an order
router.post("/create-order", auth.verify, (req, res) =>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orderId: req.body.orderId,
		quantity: req.body.quantity,
		productName: req.body.productName
	}

	if(data.isAdmin == false) {

		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false);
	}
})

// Retrieve user details
router.get("/details", auth.verify,  (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		params: req.params
	}
	
	userController.getUser(data).then(resultFromController => res.send(resultFromController));
})


// Router to get all users
router.get("/all-User", (req, res) => {

	userController.getAllUser().then(resultFromController => res.send(resultFromController));

})
module.exports = router;