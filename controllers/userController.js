const User = require("../models/User.js");
const auth = require("../auth.js")
const bcrypt = require("bcrypt");
const Product = require("../models/Product.js")
  

// Register a New User
module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
} 


// Authenticate a user
module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
}

// Retrieve user details
module.exports.getUser = (data) => {

	return User.findById(data.userId).then(result => {

		return result;
	})
}


// Create an Order
module.exports.createOrder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderedProduct.push({
			products : [
				{
					productId : data.order.productId,
					productName : data.order.productName,
					quantity : data.order.quantity
				}
			],
			totalAmount : data.order.totalAmount
		});

		return user.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.order.productId).then(product => {

		product.userOrders.push({userId: data.userId});

		return product.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}



// Get all Users
module.exports.getAllUser = () => {

	return User.find({}).then(result => {

		return result;

	});
};


// Retrieve user details
module.exports.getUser = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";

		return result;

	});

};


module.exports.checkIfEmailExists = (requestBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email: requestBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if(result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		}
	})
};




