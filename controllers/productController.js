const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")
const User = require("../models/User.js");


// Create a new product
module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name : product.name,
		description : product.description,
		price : product.price
	});

	return newProduct.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		};
	});
};

// Retrieving all products
module.exports.getAllProduct = () => {

	return Product.find({}).then(result => {

		return result;

	});
};



// Retrieving All Active Products
module.exports.getActiveProduct = () => {

	return Product.find({isActive: true}).then(result => {

		return result;

	})
};

// Retrieving a specific product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;

	})
}

// Updating a product
module.exports.updateProduct = (product, paramsId) => {

	let updatedProduct = {
		name : product.name,
		description : product.description,
		price : product.price
	}

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}

// Archive a product
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
}; 

// Reactivate a product
module.exports.reactivateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
}; 