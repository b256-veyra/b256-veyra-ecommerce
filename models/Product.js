const mongoose = require("mongoose");

// Schema for our product
const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type: String,
				required: [true, "User ID is required"]
			},

			orderId: {

				type: String,
				required: [true, "Order ID is required"]
			}
		}
	]

});


module.exports = mongoose.model("Product", productSchema);